package id.branditya.binarchapter4tugas3.model

import android.media.Image

data class Product(
    val name: String,
    val price: String,
    val image: Int
)
