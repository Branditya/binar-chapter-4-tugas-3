package id.branditya.binarchapter4tugas3.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import id.branditya.binarchapter4tugas3.R
import id.branditya.binarchapter4tugas3.model.Product

class ProductAdapter (private val listProduct : ArrayList<Product>) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>(){

    inner class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvName : TextView = view.findViewById(R.id.tv_name)
        private val tvPrice : TextView = view.findViewById(R.id.tv_price)
        private val ivImage : ImageView = view.findViewById(R.id.iv_product)

        fun bind (item : Product){
            tvName.text =item.name
            tvPrice.text =item.price
            ivImage.setImageResource(item.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view =LayoutInflater.from(parent.context).inflate(R.layout.item_product,parent,false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(listProduct[position])
    }

    override fun getItemCount(): Int = listProduct.size

}