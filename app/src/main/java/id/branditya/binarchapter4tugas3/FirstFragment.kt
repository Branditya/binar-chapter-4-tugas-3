package id.branditya.binarchapter4tugas3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import id.branditya.binarchapter4tugas3.adapter.ProductAdapter
import id.branditya.binarchapter4tugas3.databinding.FragmentFirstBinding
import id.branditya.binarchapter4tugas3.model.Product

class FirstFragment : Fragment() {
    private var _binding : FragmentFirstBinding? = null
    private val binding get() = _binding!!

    private val listProducts = arrayListOf<Product>(
        Product("Sapu","Rp. 3.000.000", R.drawable.product_sapu),
        Product("Jam","Rp. 2.000", R.drawable.product_jam)
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        val productAdapter = ProductAdapter(listProducts)
        binding.rvData.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = productAdapter
        }
    }
}